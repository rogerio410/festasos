<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Clientes</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
</head>
<body>

	<h2>Clientes</h2>
	
	<table class="table">
		<tr>
			<th>#</th>
			<th>Nome</th>
			<th>Cidade</th>
			<th>Telefones</th>
			<th>A��es</th>
		<tr>
		
		<c:forEach items="${clienteList}" var="cliente">
			<tr>
				<td>${cliente.id }</td>
				<td>${cliente.nome }</td>
				<td>${cliente.cidade }</td>
				<td>
				<c:forEach items="${cliente.telefones}" var="telefone">
					${telefone.descricao} - ${telefone.numero} <br /> 
				</c:forEach>
				</td> 
				<td><a href="${linkTo[ClienteController].edit()}?id=${cliente.id}" class="glyphicon glyphicon-pencil"></a> </td>
				<td><a href="${linkTo[ClienteController].delete()}?id=${cliente.id}" class="glyphicon glyphicon-remove"></a> </td>
			</tr>
		</c:forEach>
			
	</table>
	
	<a href="${linkTo[ClienteController].add()}" class="glyphicon glyphicon-plus"></a>

</body>
</html>