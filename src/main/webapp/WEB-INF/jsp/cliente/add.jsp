<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Cliente</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
</head>
<body>

	<h2>Novo Cliente</h2>

	<form action="${linkTo[ClienteController].save()}">
		<div class="form-group">
			<label>Nome</label> 
			<input name="cliente.nome" 	type="text" class="form-control" placeholder="Digite seu nome" />
			<input name="cliente.cidade" 	type="text" class="form-control" placeholder="Cidade" />
			
			<label>Telefone:</label>
			<select name="telefone.descricao">
				<option value="Fixo">Fixo</option>
				<option value="Celular">Fixo</option>
			</select>

			<input type="text" name="telefone.numero" placeholder="(86) 9999-9999" />
			
			<input type="submit" class="btn btn-success" />
		</div>
	</form>


</body>
</html>