package br.com.caelum.vraptor.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name="Cliente.TodosOrdenadoPorNome", 
			query="select c from Cliente c order by c.nome"),
	@NamedQuery(name="Cliente.PorCidade",
			query="SELECT c FROM Cliente c WHERE c.cidade = :city"),
	@NamedQuery(name="Cliente.TodosOrdenadoPorCidade", 
			query="SELECT c FROM Cliente c order by c.cidade"),
})
public class Cliente {
	
	//Consultas Nomeadas
	public static String CLIENTE_PORCIDADE = "Cliente.PorCidade";
	public static String CLIENTE_TODOS_ORDENADO_POR_NOME = "Cliente.TodosOrdenadoPorNome";
	public static String CLIENTE_TODOS_ORDENADO_POR_CIDADE = "Cliente.TodosOrdenadoPorCidade";
	
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private String cidade;
	private String telefone;
	
	@OneToMany(mappedBy="cliente", cascade=CascadeType.PERSIST)
	private List<Telefone> telefones = new ArrayList<Telefone>();
	
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Cliente() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
	
	

}
