package br.com.caelum.vraptor.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Tema {
	
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private double valorAluguel;
	private String corToalha;
	
	@OneToMany
	private List<ItemTema> itens;
	
	public Tema() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValorAluguel() {
		return valorAluguel;
	}

	public void setValorAluguel(double valorAluguel) {
		this.valorAluguel = valorAluguel;
	}

	public String getCorToalha() {
		return corToalha;
	}

	public void setCorToalha(String corToalha) {
		this.corToalha = corToalha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<ItemTema> getItens() {
		return itens;
	}

	public void setItens(List<ItemTema> itens) {
		this.itens = itens;
	}
	

}
