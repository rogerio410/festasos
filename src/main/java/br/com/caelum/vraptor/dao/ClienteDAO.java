package br.com.caelum.vraptor.dao;

import br.com.caelum.vraptor.entidades.Cliente;

public class ClienteDAO extends GenericJPADAO<Cliente>{
	
	public ClienteDAO() {
		// TODO Auto-generated constructor stub
		this.persistentClass = Cliente.class;
	}

}