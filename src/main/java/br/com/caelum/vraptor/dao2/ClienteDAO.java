package br.com.caelum.vraptor.dao2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.vraptor.entidades.Cliente;

public class ClienteDAO {
	
	EntityManagerFactory emf 
		= Persistence.createEntityManagerFactory("jpa-hibernate-mysql-dev-local");
	EntityManager em;
	
	public void salvar(Cliente c){
		em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
		em.close();
	}
	
	public void apagar(Cliente c){
		em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(c));
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Cliente> todos(){
		em = emf.createEntityManager();
		List<Cliente> result = em.createNamedQuery(Cliente.CLIENTE_TODOS_ORDENADO_POR_NOME, Cliente.class).getResultList();
		em.close();
		return result; 
	}
	
	public Cliente consultaPorId(int id){
		em = emf.createEntityManager();
		Cliente c = em.find(Cliente.class, id);
		em.close();
		return c;
	}
	
	
	public List<Cliente> consultaSemParametros(String nomeConsulta){
		em = emf.createEntityManager();
		return em.createNamedQuery(nomeConsulta, Cliente.class).getResultList();
	}
	
	public void encerra(){
		emf.close();
	}
	
	
	
	
	
	
	
	

}
