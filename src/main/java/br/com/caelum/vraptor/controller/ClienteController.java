package br.com.caelum.vraptor.controller;

import java.util.List;

import javax.inject.Inject;






import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.ClienteDAO;
import br.com.caelum.vraptor.entidades.Cliente;
import br.com.caelum.vraptor.entidades.Telefone;

@Controller
public class ClienteController {
	
	@Inject
	private Result result;
	
	@Inject
	private ClienteDAO clienteDAO;
	
	public void add(){
		
	}
	
	public Cliente edit(int id){
		Cliente cliente = clienteDAO.find(id);
		return cliente;
	}
	
	public void save(Cliente cliente, Telefone telefone){
		
		System.out.println("Telefone: " + telefone.getNumero());
		
		cliente.getTelefones().add(telefone);
		telefone.setCliente(cliente);
		
		clienteDAO.persist(cliente);
		result.redirectTo(this).list();
	}
	
	public void update(Cliente cliente){
		clienteDAO.save(cliente);
		result.redirectTo(this).list();	
	}
	
	public List<Cliente> list(){
		return clienteDAO.find();
	}
	
	public void delete(int id){
		System.out.println("Action: Delete \n" );
		
		Cliente cliente = new Cliente();
		cliente.setId(id);
		
		clienteDAO.delete(cliente);
		
		result.redirectTo(this).list();
	}
	

}
