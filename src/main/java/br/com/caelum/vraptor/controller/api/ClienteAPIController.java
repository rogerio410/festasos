package br.com.caelum.vraptor.controller.api;

import javax.inject.Inject;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.ClienteDAO;
import br.com.caelum.vraptor.entidades.Cliente;
import br.com.caelum.vraptor.serialization.gson.WithoutRoot;
import br.com.caelum.vraptor.view.Results;

@Controller
public class ClienteAPIController {
	
	@Inject
	private Result result;
	
	@Inject
	private ClienteDAO clienteDAO;
	
	@Get("/clientes")
	public void list(){
		result.use(Results.json()).withoutRoot()
			.from(clienteDAO.find()).serialize();
	}
	
	
	@Post("/clientes")
	@Consumes(value="application/json", options=WithoutRoot.class)
	public void save(Cliente cliente){
		clienteDAO.save(cliente);
		result.use(Results.status()).ok();
	}
	
	
	@Delete("/clientes/{cliente.id}")
	public void delete(Cliente cliente){
		try{
			clienteDAO.delete(cliente);
			result.use(Results.status()).ok();
			clienteDAO.commit();
		}catch(Exception e){
			result.use(Results.status()).badRequest("Não é possível apagar este Cliente");
		}
		
	}
	
}
