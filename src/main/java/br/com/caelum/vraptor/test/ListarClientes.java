package br.com.caelum.vraptor.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.entidades.Cliente;

public class ListarClientes {
	
	public static void main(String[] args) {
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-mysql-dev-local");
			EntityManager em = emf.createEntityManager();
			
			//Montar a Consulta
			TypedQuery<Cliente> query = 
					em.createQuery("SELECT c FROM Cliente c", Cliente.class);
			
			//Executar a Consulta
			List<Cliente> clientes = query.getResultList();
			
			//Exibir clientes
			for (Cliente cliente : clientes) {
				System.out.println("Cliente: " + cliente.getNome());
			}
			
			em.close();
			emf.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
