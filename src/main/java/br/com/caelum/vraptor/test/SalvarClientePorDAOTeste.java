package br.com.caelum.vraptor.test;

import br.com.caelum.vraptor.dao2.ClienteDAO;
import br.com.caelum.vraptor.entidades.Cliente;

public class SalvarClientePorDAOTeste {
	
	public static void main(String[] args) {
		
		ClienteDAO dao = new ClienteDAO();
		
		Cliente c1 = new Cliente();
		c1.setNome("Josueh Fernando");
		c1.setCidade("Angical-PI");
		
		dao.salvar(c1);
		
	}

}
