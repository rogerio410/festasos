package br.com.caelum.vraptor.test;

import br.com.caelum.vraptor.dao2.ClienteDAO;
import br.com.caelum.vraptor.entidades.Cliente;

public class RemoverClientePorDAOTeste {
	
	public static void main(String[] args) {
		
		ClienteDAO dao = new ClienteDAO();
		
		Cliente c1 = new Cliente();
		c1.setId(38);
		
		dao.apagar(c1);
		
	}

}
