package br.com.caelum.vraptor.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.vraptor.entidades.Cliente;
import br.com.caelum.vraptor.entidades.Telefone;

public class TestClienteTelefone {
	
	public static void main(String[] args) {
		
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-hibernate-mysql-dev-local");
			EntityManager em = emf.createEntityManager();
			
			em.getTransaction().begin();
			
			Telefone t = new Telefone();
			t.setNumero("99999999");
			
			Cliente c = new Cliente();
			c.setNome("Breno2");
			
			//Fechando os dois lado do relacionamento
			c.getTelefones().add(t);
			t.setCliente(c);
			
			//Só preciso persistir um os lados que relacionamento
			em.persist(c);
			
			em.getTransaction().commit();
			em.close();
			emf.close();
			System.out.println("Banco de Dados Atualizado...");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro ao atualizar banco de dados..");
		}
		
	}

}
