package br.com.caelum.vraptor.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.caelum.vraptor.dao2.ClienteDAO;
import br.com.caelum.vraptor.entidades.Cliente;

public class ListarClientespeloDAO {
	
	public static void main(String[] args) {
		
		ClienteDAO dao = new ClienteDAO();
			
		//Executar a Consulta
		List<Cliente> clientes = dao.consultaSemParametros(Cliente.CLIENTE_TODOS_ORDENADO_POR_CIDADE);
		
		//Exibir clientes
		for (Cliente cliente : clientes) {
			System.out.println("Cliente: " + cliente.getNome() + " / " + cliente.getCidade());
		}
		
		dao.encerra();
	}

}
